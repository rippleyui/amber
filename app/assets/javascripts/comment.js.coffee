$ ->
  $(".reply").bind 'click', (event) ->
    reply = $(this).data('reply-to') + '\n'
    current_content = $("#comment_content").val()
    $("#comment_content").focus()
    $("#comment_content").val(current_content + reply)