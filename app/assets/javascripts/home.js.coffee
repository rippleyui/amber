$ ->
  $(".img-wrapper").bind 'mouseenter', (event) ->
    $(this).find(".img-caption-photo-title").hide()
    $(this).find(".img-caption-photo-user").hide().fadeIn()
  $(".img-wrapper").bind 'mouseleave', (event) ->
    $(this).find('.img-caption-photo-title').hide().fadeIn()
    $(this).find('.img-caption-photo-user').hide()