$ ->
  $("#img-exif").bind 'mouseover', (event) ->
    hasValue = $("#exif-details li").length
    if hasValue > 0
      $("#exif-details").children("li").fadeIn()
    else
      loading = "<li>正在努力读取EXIF中……</li>"
      $("#exif-details").append(loading).show()
      file = $("#exif-link").html()
      $.getJSON file, (data) ->
        items = []
        if data.EXIF
          $.each data.EXIF, (key, val) ->
            switch key
              when 'Make'
                items.push( "<li>" + '厂商：' + val + "</li>" )
              when 'Model'
                items.push( "<li>" + '型号：' + val + "</li>" )
              when 'Software'
                items.push( "<li>" + '处理软件：' + val + "</li>" )
              when 'ExposureTime'
                value = val + 'secs'
                items.push( "<li>" + '快门：' + value + "</li>" )
              when 'FNumber'
                value = "f/" + eval(val)
                items.push( "<li>" + '光圈：' + value + "</li>" )
              when 'ISOSpeedRatings'
                items.push( "<li>" + 'ISO：' + val + "</li>" )
              when 'FocalLength'
                value = eval(val) + 'mm';
                items.push( "<li>" + '焦距：' + value + "</li>" )
        else
          items.push("<li>无EXIF信息：（</li>")
        $("#exif-details").html('')
        $("#exif-details").append(items).hide().fadeIn()

  $("#img-exif").bind 'mouseleave', (event) ->
    $("#exif-details").children("li").hide()

  $("#prev").bind 'mouseenter', (event) ->
    $(this).hide().fadeIn()

  $("#next").bind 'mouseenter', (event) ->
    $(this).hide().fadeIn()