$ ->
  $('#user_avatar').bind 'change', (event) ->
    file = this.files[0]
    allowedExtension = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"]
    fileSize = file.size
    extName = file.name.split('.').pop()
    if fileSize > 2097152
      $('input[type="submit"]').attr('disabled','disabled')
      alert("文件大于2M，请重新选择小点的上传，谢谢！")
    else
      if $.inArray(extName, allowedExtension) == -1
        $('input[type="submit"]').attr('disabled','disabled')
        alert("文件格式不对，请重新选择图片上传，谢谢！")
      else
        $('input[type="submit"]').removeAttr('disabled')

  $('#photo_file_name').bind 'change', (event) ->
    file = this.files[0]
    allowedExtension = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"]
    fileSize = file.size
    extName = file.name.split('.').pop()
    if fileSize > 10485760
      $('input[type="submit"]').attr('disabled','disabled')
      alert("文件大于10M，请重新选择小点的上传，谢谢！")
    else
      if $.inArray(extName, allowedExtension) == -1
        $('input[type="submit"]').attr('disabled','disabled')
        alert("文件格式不对，请重新选择图片上传，谢谢！")
      else
        $('input[type="submit"]').removeAttr('disabled')