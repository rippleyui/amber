class Admin::CommentsController < ApplicationController
  layout 'admin'
  before_action :admin_user?, only: [:index, :destroy]
  before_action :set_photo, only: [:index, :destroy]

  def index
    @comments = @photo.comments.includes(:user).order("created_at DESC").page params[:page]
  end

  def destroy
    @comment = @photo.comments.find(params[:id])
    if @comment.destroy
      redirect_to admin_photo_comments_path, notice: "成功删除评论！"
    else
      flash.now.alert = "删除评论失败！"
      redirect_to admin_photo_comments_path
    end
  end

  private
    def set_photo
      @photo = Photo.find(params[:photo_id])
    end
end
