class Admin::HomeController < ApplicationController
  layout 'admin'
  before_action :admin_user?

  def index
    @users_count = User.count
    @photos_count = Photo.count
    @comments_count = Comment.count
    @pages_count = Page.count
    @tips_count = Tip.count
  end
end
