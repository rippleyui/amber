class Admin::PagesController < ApplicationController
  layout 'admin'
  before_action :admin_user?, only: [:index, :new, :create, :edit, :destroy]
  before_action :set_page, only: [:edit, :update, :destroy]

  def index
    @pages = Page.order("created_at DESC").page params[:page]
  end

  def new
    @page = Page.new
  end

  def create
    @page = Page.new(page_params)
    if @page.save
      redirect_to @page, notice: "创建页面成功！"
    else
      flash.now.alert = "创建页面失败..."
      render :new
    end
  end

  def edit
  end

  def update
    if @page.update(page_params)
      redirect_to page_path(@page), notice: "编辑页面成功！"
    else
      flash.now.alert = "编辑页面失败..."
      render :edit
    end
  end

  def destroy
    if @page.destroy
      redirect_to admin_pages_path, notice: "删除页面成功！"
    else
      flash.now.alert = "删除页面失败..."
      render :edit
    end
  end

  private
    def page_params
      params.require(:page).permit(:slug, :title, :content, :created_at, :updated_at)
    end

    def set_page
      @page = Page.find_by_slug(params[:id])
    end
end
