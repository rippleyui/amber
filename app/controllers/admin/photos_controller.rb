class Admin::PhotosController < ApplicationController
  layout 'admin'
  before_action :admin_user?, only: [:index, :destroy]

  def index
    @photos = Photo.all.includes(:user).order('created_at DESC').page params[:page]
    @photos_count = Photo.all.count
  end

  def destroy
    @photo = Photo.find(params[:id])
    if @photo.destroy
      redirect_to admin_photos_path, notice: "成功删除照片！"
    else
      flash.now.alert = "删除照片失败！"
      redirect_to admin_photos_path
    end
  end
end
