class Admin::TipsController < ApplicationController
  layout 'admin'
  before_action :admin_user?, only: [:index, :new, :create, :edit, :destroy]

  def index
    @tips = Tip.order("created_at DESC").page params[:page]
  end

  def new
    @tip = Tip.new
  end

  def create
    @tip = Tip.new(tip_params)
    if @tip.save
      redirect_to admin_tips_path, notice: "创建tip成功！"
    else
      flash.now.alert = "创建tip失败..."
      render :new
    end
  end

  def edit
    @tip = Tip.find(params[:id])
  end

  def update
    @tip = Tip.find(params[:id])
    if @tip.update(tip_params)
      redirect_to tip_path(@tip), notice: "编辑tip成功！"
    else
      flash.now.alert = "编辑tip失败..."
      render :edit
    end
  end

  def destroy
    @tip = Tip.find(params[:id])
    if @tip.destroy
      redirect_to admin_tips_path, notice: "删除tip成功！"
    else
      flash.now.alert = "删除tip失败..."
      render :edit
    end
  end

  private
    def tip_params
      params.require(:tip).permit(:body, :created_at, :updated_at)
    end
end
