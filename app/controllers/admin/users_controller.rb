class Admin::UsersController < ApplicationController
  layout 'admin'
  before_action :admin_user?, only: [:index, :destroy]

  def index
    @users = User.all.order("created_at ASC").page params[:page]
    @users_count = User.all.count
  end

  def destroy
    @user = User.find_by(uuid: params[:id])
    if @user.destroy
      redirect_to admin_users_path, notice: "成功删除用户！"
    else
      flash.now.alert = "删除用户失败！"
      redirect_to admin_users_path
    end
  end
end