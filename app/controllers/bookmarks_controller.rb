class BookmarksController < ApplicationController
  before_action :signed_in_user, only: [:create, :destroy]

  def index
    @user = User.find_by_uuid(params[:user_id])
    @photos = Kaminari.paginate_array(@user.bookmarked_photos).page(params[:page])
    render layout: "profile"
  end

  def create
    @bookmarkable = find_bookmarkable
    @bookmark = @bookmarkable.bookmarks.build
    @bookmark.user = current_user
    if @bookmark.save
      redirect_to @bookmarkable, notice: "收藏成功！"
    else
      flash.now.alert = "收藏失败！"
      redirect_to @bookmarkable
    end
  end

  def destroy
    @bookmark = Bookmark.find(params[:id])
    @bookmarkable = @bookmark.bookmarkable
    if @bookmark.destroy
      redirect_to @bookmarkable, notice: "取消收藏成功！"
    else
      flash.now.alert = "取消收藏失败！"
      redirect_to @bookmarkable
    end
  end

  private
    def find_bookmarkable
      params.each do |name, value|
        if name =~ /(.+)_id$/
          return $1.classify.constantize.find(value)
        end
      end
      nil
    end
end
