class CategoriesController < ApplicationController
  def show
  	@category = Category.find_by(name: params[:id])
  	@photos = @category.photos.includes(:user).order('created_at DESC').page params[:page]
  end
end
