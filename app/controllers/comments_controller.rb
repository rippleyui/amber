class CommentsController < ApplicationController
	before_action :signed_in_user, only: [:create, :destroy]

	def create
    @commentable = find_commentable
    @comment = @commentable.comments.build(comment_params)
    @comment.user = current_user
    if @comment.save
      respond_to do |format|
        format.html { redirect_to @commentable, notice: "创建评论成功！" }
        format.js
      end
    else
      flash.now.alert = "评论失败！"
      redirect_to @commentable
    end
	end

	def destroy
    @comment = current_user.comments.find(params[:id])
    @commentable = @comment.commentable
    if @comment.destroy
      respond_to do |format|
        format.html { redirect_to @commentable, notice: "删除评论成功！" }
        format.js
      end
    else
      flash.now.alert = "删除评论失败！"
      redirect_to @commentable
    end
	end

	private
	  def comment_params
      params.require(:comment).permit(:content, :commentable_type, :commentable_id, :user_id)
	  end

    def find_commentable
      params.each do |name, value|
        if name =~ /(.+)_id$/
          return $1.classify.constantize.find(value)
        end
      end
      nil
    end
end
