class HomeController < ApplicationController
  def index
    if signed_in?
      redirect_to photos_path and return
    end
    render :layout => 'home'
  end

  def photos
    @photos = Photo.order('created_at DESC').includes(:user).page params[:page]
    render :layout => 'application'
  end
end
