class NotificationsController < ApplicationController
  before_action :signed_in_user, only: [:index, :read, :viewed, :clear]

  def index
    @notifications = current_user.notifications.where(:unread => true).includes(:action_user).order('created_at DESC')
    @viewed = current_user.notifications.where(:unread => false).includes(:action_user).order('created_at DESC').limit(25)
  end

  def read
    @notification = current_user.notifications.find(params[:id])
    if @notification.present?
      @notification.update(:unread => false)
      redirect_to @notification.notifiable.notifiable_path
    else
      redirect_to root_path, :alert => "无法处理之前的请求！"
    end
  end

  def viewed
    if current_user.notifications.where(:unread => true).update_all(:unread => false)
      redirect_to notifications_path, notice: "未读消息已全部标记为已读。"
    else
      flash.now.alert = "全部标记为已读失败！"
      render :index
    end
  end

  def clear
    if current_user.notifications.destroy_all
      redirect_to root_path, notice: "已清空消息。"
    else
      flash.now.alert = "清空失败！"
      render :index
    end
  end
end
