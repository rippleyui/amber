class PhotosController < ApplicationController
  before_action :signed_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :set_photo, only: [:edit, :update, :destroy]

  def new
  	@photo = current_user.photos.new
  end

  def create
    @photo = current_user.photos.new(photo_params)
    if @photo.save
    	redirect_to @photo, notice: "上传成功啦:)"
    else
    	flash.now.alert = "上传失败，请重传:)"
    	render :new
    end
  end

  def show
  	@photo = Photo.find(params[:id])
    @recent_photos = @photo.user.photos.order("created_at DESC").limit(8)
    @prev = @photo.prev_photo
    @next = @photo.next_photo
    @commentable = @photo
    @comments = @commentable.comments.includes(:user).order("created_at ASC")
    @comment = Comment.new
    @tip = Tip.order("RANDOM()").limit(1).first
  end

  def edit
  end

  def update
    if @photo.update(photo_params)
      redirect_to @photo, notice: "更新照片成功！"
    else
      flash.now.alert = "更新失败：（"
      render :edit
    end
  end

  def destroy
    @photo.destroy
    redirect_to current_user, notice: "删除照片成功！"
  end

  private
    def photo_params
      params.require(:photo).permit(:file_name, :title, :body, :category_name, :license, :created_at, :updated_at)
    end

    def set_photo
      @photo = current_user.photos.find(params[:id])
    end
end
