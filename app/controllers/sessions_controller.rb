class SessionsController < ApplicationController
  def new
  end

  def create
	  auth = request.env["omniauth.auth"]
    old_or_new_user(auth)
  end

  def destroy
	  cookies.delete(:secure_user_id)
    cookies.delete(:auth_token)
    redirect_to root_url, notice: "已退出！"
  end

  def failure
    redirect_to root_url, alert: "登录失败，请重试！"
  end

  private
    def add_token(user)
      auth_token = User.new_auth_token
      cookies.permanent[:auth_token] = auth_token
      user.update_attribute(:auth_token, User.encrypt(auth_token))
      cookies.signed[:secure_user_id] = {secure: true, value: "secure#{user.id}"}
    end

    def old_or_new_user(auth)
      if (old_user = User.find_by_provider_and_uid(auth["provider"], auth["uid"]))
        add_token(old_user)
        redirect_to photos_path, notice: "欢迎，#{old_user.name}！"
      else
        new_user = User.create_with_omniauth(auth)
        add_token(new_user)
        session[:user_id] = new_user.id
        redirect_to setup_user_path(new_user)
      end
    end
end
