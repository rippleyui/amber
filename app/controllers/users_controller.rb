class UsersController < ApplicationController
  before_action :signed_in_user, only: [:edit, :update]
  before_action :set_user, only: [:edit, :update]
  before_action :set_new_user, only: [:setup, :update]

  def show
  	@user = User.find_by(uuid: params[:id])
  	@photos = @user.photos.order('created_at DESC').page params[:page]
    render layout: "profile"
  end

  def edit
  end

  def update
    if @user.update(user_params)
      session[:user_id] = nil if session[:user_id]
      redirect_to @user, notice: "更新成功！"
    else
      if session[:user_id].nil?
        flash.now.alert = "更新失败！"
        render :edit
      else
        flash.now.alert = "更新失败！"
        render :setup, :layout => 'home'
      end
    end
  end

  def setup
    flash.now.notice = "请先上传头像和填写Email，谢谢！"
    render :layout => 'home'
  end

  def info
    @user = User.includes(:account).find_by(uuid: params[:id])
    render layout: "profile"
  end

  private
    def user_params
      params.require(:user).permit(:avatar, :email, account_attributes: [:signature, :url, :bio])
    end

    def set_user
      @user = current_user
    end

    def set_new_user
      @new_user = User.find_by(id: session[:user_id])
    end
end
