module Admin::UsersHelper
  def normal_or_admin(user)
    if user.admin?
      "管理员"
    else
      link_to "删除", admin_user_path(user), method: :delete, data: { confirm: '你确定删除该用户？再想想？' }
    end
  end
end
