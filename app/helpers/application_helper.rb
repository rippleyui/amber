module ApplicationHelper
  class HTMLwithCodeRay < Redcarpet::Render::HTML
    def block_code(code, language)
      CodeRay.scan(code, language).div(:tab_width=>2)
    end
  end

	def markdown(text)
    options = {
        :autolink => true,
        :space_after_headers => true,
        :fenced_code_blocks => true,
        :no_intra_emphasis => true,
        :hard_wrap => true,
        :strikethrough =>true,
        :tables => true,
        :highlight => true,
        :footnotes => true
      }
    markdown = Redcarpet::Markdown.new(HTMLwithCodeRay, options)
    markdown.render(text).html_safe
  end

  def format_time(time)
    time.strftime("%F %T")
  end
end
