module CommentsHelper
  def format_text(text)
    simple_format(text)
  end

  def format_comment(text)
    truncate(text, length: 40)
  end
end