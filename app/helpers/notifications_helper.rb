module NotificationsHelper
  def unread_notification_count
    @unread_count = current_user.notifications.where(:unread => true).count
  end

  def format_notification(text)
    truncate(text, length: 25)
  end
end
