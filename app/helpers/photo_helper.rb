module PhotoHelper
	def exif(photo)
    file = photo.file_name
    "#{file}!exif"
  end

  def resize(photo, size)
    image_tag "#{photo.file_name}!#{size}"
  end

  def photo_filename(photo, size)
    "#{photo.file_name}!#{size}"
  end

  def share_title(photo)
    "#{photo.title} by #{photo.user.name}"
  end

  def meta_twitter_card(property, content)
    tag("meta", name: "twitter:#{property}", content: "#{content}")
  end
end
