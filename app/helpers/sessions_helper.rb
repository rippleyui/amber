module SessionsHelper
  def current_user
    if !request.ssl? || cookies.signed[:secure_user_id] == "secure#{session[:user_id]}"
      auth_token = User.encrypt(cookies[:auth_token])
      @current_user ||= User.find_by_auth_token(auth_token) if cookies[:auth_token]
    end
  end

  def signed_in?
  	!current_user.nil?
  end

  def signed_in_user
    redirect_to root_path, alert: "请先登录！" unless signed_in?
  end

  def current_user?(user)
    user == current_user
  end

  def admin_user?
    not_found unless current_user.admin?
  end

  private
    def not_found
      raise ActionController::RoutingError.new('Not Found')
    end
end