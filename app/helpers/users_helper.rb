module UsersHelper
  def resize_avatar(user, size)
    image_tag "#{user.avatar}!#{size}"
  end

  def signature_or_nil(user)
    user.account.signature.empty? ? "这家伙居然不写签名，太没个性了。。。" : user.account.signature
  end

  def bio_or_nil(user)
    user.account.bio.empty? ? "这家伙太懒了，都不肯介绍下自己。。。" : user.account.bio
  end

  def url_or_nil(user)
    user.account.url.empty? ? "施主，要不留下你的微博或豆瓣或其他地址，方便以后交流嘛！" : user.account.url
  end
end
