class Account < ActiveRecord::Base
  belongs_to :user
  before_create :set_default_value

  validates :signature, length: { maximum: 50 }
  validates :url, length: { maximum: 100 }
  validates :bio, length: { maximum: 200 }

  private
    def set_default_value
      self.signature ||= ''
      self.url ||= ''
      self.bio ||= ''
    end
end