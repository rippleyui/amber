class Bookmark < ActiveRecord::Base
  belongs_to :user
  belongs_to :bookmarkable, :polymorphic => true

  validates :user_id, :bookmarkable_type, :bookmarkable_id, :presence => true
end
