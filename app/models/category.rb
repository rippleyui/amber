class Category < ActiveRecord::Base
	PHOTO_TYPES = ['风光','人文','黑白','动物','其他','纪实','人像']
	has_many :photos

	def to_param
  	name
  end
end
