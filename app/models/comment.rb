class Comment < ActiveRecord::Base
  MENTION_REGEXP = /@([a-zA-Z0-9_\-\p{han}]+)/u

	belongs_to :user
	belongs_to :commentable, :polymorphic => true, :counter_cache => true

  validates :user_id, :commentable_id, :commentable_type, :content, :presence => true
	validates :content, length: { minimum: 5 }

  after_create :send_notifications

  def mentioned_users
    mentioned_names = self.content.scan(MENTION_REGEXP).collect {|matched| matched.first}.uniq
    mentioned_names.delete(self.user.name)
    mentioned_names.map {|name| User.find_by_name(name)}.compact
  end

  private
    # send notification to commentable owner unless the comment was created by the same owner
    def send_notifications
      send_notification_to(self.commentable.user, Notification::ACTION_REPLY) unless self.user == self.commentable.user
      send_notification_to_mentioned_users
    end

    def send_notification_to(user, action)
      Notification.notify(user, self.commentable, self.user, action, self.content)
    end

    def send_notification_to_mentioned_users
      mentioned_users.each do |user|
        send_notification_to(user, Notification::ACTION_MENTION)
      end
    end
end