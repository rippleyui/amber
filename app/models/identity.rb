class Identity < OmniAuth::Identity::Models::ActiveRecord
	VALID_NAME_REGEX = /\A[a-zA-Z][a-zA-Z0-9]/
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\Z/i
	validates :name, presence: true,
                   uniqueness: { case_sensitive: false },
                   length: { minimum: 4, maximum: 16 },
                   format: { with: VALID_NAME_REGEX }
  validates :email, presence: true,
                    uniqueness: { case_sensitive: false },
                    format: { with: VALID_EMAIL_REGEX }
  validates :password, length: { minimum: 8 }
end
