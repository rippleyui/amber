class Notification < ActiveRecord::Base
  ACTION_MENTION = 'mention'
  ACTION_REPLY = 'reply'

  belongs_to :user
  belongs_to :action_user, :class_name => 'User'
  belongs_to :notifiable, :polymorphic => true

  # Notify user
  def self.notify(user, notifiable, action_user, action, content)
    notification = Notification.new(:action => action, :content => content)
    notification.notifiable = notifiable
    notification.user = user
    notification.action_user = action_user
    notification.save
  end

  def action_info_prefix
    case self.action
    when ACTION_MENTION
      '在回复'
    when ACTION_REPLY
      '在'
    end
  end

  def action_info_suffix
    case self.action
    when ACTION_MENTION
      '时提到'
    when ACTION_REPLY
      '里回复'
    end
  end
end
