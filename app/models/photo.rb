class Photo < ActiveRecord::Base
  LICENSES = ['姓名标示-非商业性-禁止改动', '姓名标示-非商业性-相同方式分享', '姓名标示-非商业性', '姓名标示-禁止改动', '姓名标示-相同方式分享', '姓名标示']
	belongs_to :user
	belongs_to :category
  has_many :comments, :as => :commentable, :dependent => :destroy
  has_many :bookmarks, :as => :bookmarkable, :dependent => :destroy

	mount_uploader :file_name, ImageUploader

  validates :title, presence: true,
                    length: { minimum: 1 }
  validates :body, presence: true,
                   length: { minimum: 1 }
  validates :file_name, presence: true
  validates :category_name, presence: true

  def category_name
    self.category ? self.category.name : self.category
  end

  def category_name=(c_name)
    self.category = Category.find_or_create_by(name: c_name)
  end

  def notifiable_path
    "/photos/#{id}"
  end

  def prev_photo
    Photo.where(['id < ?', self.id]).order('created_at DESC').first
  end

  def next_photo
    Photo.where(['id > ?', self.id]).order('created_at ASC').first
  end
end
