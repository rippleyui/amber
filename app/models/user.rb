class User < ActiveRecord::Base
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\Z/i
  has_one :account, :dependent => :destroy
  has_many :photos, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  has_many :notifications, :dependent => :destroy
  has_many :bookmarks, :dependent => :destroy

  mount_uploader :avatar, ImageUploader
  before_create :create_account
  before_create :create_auth_token
  before_create :create_uuid

  validates :email, on: :update,
                    presence: true,
                    uniqueness: { case_sensitive: false },
                    format: { with: VALID_EMAIL_REGEX }

  accepts_nested_attributes_for :account

	def self.create_with_omniauth(auth)
    create! do |user|
      user.provider = auth["provider"]
      user.uid = auth["uid"]
      user.name = auth["info"]["name"]
    end
	end

	def to_param
		uuid
	end

  def User.new_auth_token
    SecureRandom.urlsafe_base64
  end

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  def bookmarked?(bookmarkable)
    bookmarkable.bookmarks.where(:user_id => self.id).exists?
  end

  def bookmark_of(bookmarkable)
    bookmarkable.bookmarks.where(:user_id => self.id).first
  end

  def bookmarked_photos
    ids = self.bookmarks.order('created_at DESC').select(:bookmarkable_id).where(:bookmarkable_type => 'Photo').collect(&:bookmarkable_id)
    Photo.find(ids)
  end

  def number_of_bookmarked_photos
    self.bookmarks.select(:bookmarkable_id).where(:bookmarkable_type => 'Photo').count
  end

  def number_of_photos
    Photo.select(:user_id).where(:user_id => self.id).count
  end

	private
	  def create_account
      self.build_account if self.account.nil?
	  end

    def create_auth_token
      self.auth_token = User.encrypt(User.new_auth_token)
    end

    def create_uuid
      begin
        self.uuid = [*'0'..'9'].sample(8).join
      end while User.exists?(self.uuid)
    end
end
