# coding: utf-8
require "digest/md5"
require 'carrierwave/processing/mini_magick'
class ImageUploader < CarrierWave::Uploader::Base

  storage :upyun

  def store_dir
    "#{model.class.to_s.underscore}/#{mounted_as}"
  end

  def default_url
    # 搞一个大一点的默认图片取名 blank.png 用 FTP 传入图片空间，用于作为默认图片
    # Setting.upload_url 这个是你的图片空间 URL
    "http://ripple-amber.b0.upaiyun.com/default.png"
  end

  def extension_white_list
    %w(jpg jpeg png JPG JPEG PNG)
  end

  def filename
    if original_filename
      @name ||= Digest::MD5.hexdigest(File.dirname(current_path))
      "#{@name}.#{file.extension}"
    end
  end
end
