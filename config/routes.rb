Amber::Application.routes.draw do
  root 'home#index'
  get 'photos' => 'home#photos'
  get 'settings' => 'users#edit', as: :settings

  controller :sessions do
    #get '/signin' => :new, as: :signin
    get '/signout' => :destroy, as: :signout
    get '/auth/:provider/callback' => :create
    get '/auth/failure' => :failure
  end

  resources :sessions, only: [:new, :create, :destroy, :failure]
  resources :users, only: [:show, :edit, :update] do
    get :setup, :on => :member
    get :info, :on => :member, as: :info
    resources :photos, only: [:index]
    resources :bookmarks, only: [:index]
  end
  resources :photos, except: :index do
    resources :comments, only: [:create, :destroy]
    resources :bookmarks, only: [:create, :destroy]
  end
  resources :categories, only: :show
  resources :pages, only: :show
  resources :comments, only: [:create, :destroy]
  resources :bookmarks, only: [:create, :destroy]
  resources :notifications, only: [:index, :read, :viewed, :clear] do
    member do
      get :read
    end

    collection do
      get :viewed
      get :clear
    end
  end

  namespace :admin do
    root 'home#index'
    resources :users, only: [:index, :destroy]
    resources :photos, only: [:index, :destroy] do
      resources :comments, only: [:index, :destroy]
    end
    resources :pages, except: :show
    resources :tips, except: :show
  end

  # resources :identities, only: :new

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable
end