class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :signature
      t.string :url
      t.text :bio

      t.timestamps
    end
  end
end
