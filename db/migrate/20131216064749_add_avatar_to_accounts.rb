class AddAvatarToAccounts < ActiveRecord::Migration
  def change
  	add_column :accounts, :avatar, :string
  	add_index :accounts, :avatar
  end
end
