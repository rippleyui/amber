class AddCategoryIdToPhotoPosts < ActiveRecord::Migration
  def change
  	add_column :photos, :category_id, :integer
  	add_index :photos, :category_id
  	add_index :photos, :user_id
  end
end
