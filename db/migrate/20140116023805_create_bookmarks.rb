class CreateBookmarks < ActiveRecord::Migration
  def change
    create_table :bookmarks do |t|
      t.integer :user_id
      t.string :bookmarkable_type
      t.integer :bookmarkable_id

      t.timestamps
    end

    add_index :bookmarks, :user_id
    add_index :bookmarks, :bookmarkable_id
    add_index :bookmarks, :bookmarkable_type
    add_index :bookmarks, [:bookmarkable_id, :bookmarkable_type]
  end
end
