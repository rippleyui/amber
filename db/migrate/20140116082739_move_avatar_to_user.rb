class MoveAvatarToUser < ActiveRecord::Migration
  def change
    remove_column :accounts, :avatar, :string
    add_column :users, :avatar, :string
  end
end
