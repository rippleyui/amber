class AddMuchMoreIndexToNotifications < ActiveRecord::Migration
  def change
    add_index :notifications, :action_user_id
  end
end
