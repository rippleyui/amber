class AddLicenseToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :license, :string
  end
end
