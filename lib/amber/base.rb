require 'cgi'

module Amber
  module Base
    def self.make_mention_links(text)
      text.gsub(Comment::MENTION_REGEXP) do
        if $1.present?
          %(@<a href="/users/#{$1}">#{$1}</a>)
        else
          "@#{$1}"
        end
      end
    end
  end
end