require 'test_helper'

class Admin::HomeControllerTest < ActionController::TestCase
  test "should raise RoutingError if not admin when enter background" do
    user = users(:normal)
    signin(user)
    assert_raises(ActionController::RoutingError) {
      get :index
    }
  end

  test "should get index if admin when enter background" do
    user = users(:admin)
    signin(user)
    get :index
    assert_response :success
  end

  private
    def signin(user)
      cookies[:auth_token] = user.name
    end
end
