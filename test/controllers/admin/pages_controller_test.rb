require 'test_helper'

class Admin::PagesControllerTest < ActionController::TestCase
  def setup
    user = users(:admin)
    signin(user)
  end

  test "should get index if admin" do
    get :index
    assert_response :success
  end

  test "should get edit if admin" do
    page = pages(:about)
    get :edit, id: page.slug
    assert_response :success
  end

  test "should update if admin" do
    page = pages(:about)
    assert page.update(content:"updated content")
  end

  test "should destroy if admin" do
    page = pages(:about)
    assert page.destroy
  end

  private
    def signin(user)
      cookies[:auth_token] = user.name
    end
end
