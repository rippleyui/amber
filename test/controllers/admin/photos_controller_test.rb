require 'test_helper'

class Admin::PhotosControllerTest < ActionController::TestCase
  def setup
    user = users(:admin)
    signin(user)
  end

  test "should get index if admin" do
    get :index
    assert_response :success
  end

  private
    def signin(user)
      cookies[:auth_token] = user.name
    end
end
