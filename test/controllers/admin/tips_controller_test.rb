require 'test_helper'

class Admin::TipsControllerTest < ActionController::TestCase
  def setup
    user = users(:admin)
    signin(user)
  end

  test "should get index if admin" do
    get :index
    assert_response :success
  end

  test "should get edit if admin" do
    tip = tips(:one)
    get :edit, id: tip.id
    assert_response :success
  end

  test "should update if admin" do
    tip = tips(:one)
    assert tip.update(body:"updated content")
  end

  test "should destroy if admin" do
    tip = tips(:one)
    assert tip.destroy
  end

  private
    def signin(user)
      cookies[:auth_token] = user.name
    end
end
