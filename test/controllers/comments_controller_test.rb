require 'test_helper'

class CommentsControllerTest < ActionController::TestCase

  test "should fail to create comment as length is too less" do
    comment = comments(:less)
    assert_not comment.save
  end

  test "should create comment" do
    comment = comments(:regular)
    assert comment.save
  end

  test "should destroy comment" do
    comment = comments(:regular)
    assert comment.destroy
  end
end
