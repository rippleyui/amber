require 'test_helper'

class NotificationsControllerTest < ActionController::TestCase

  test "should get index and clear all unread notifications" do
    user = users(:admin)
    signin(user)
    unreads = user.notifications.where(:unread => true)
    assert notifications
    get :index
    assert_equal(0, unreads.count)
  end

  private
    def signin(user)
      cookies[:auth_token] = user.name
    end
end
