require 'test_helper'

class PagesControllerTest < ActionController::TestCase

  test "should get show" do
    page = pages(:about)
    get :show, id: page.slug
    assert_response :success
  end
end