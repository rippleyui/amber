require 'test_helper'

class PhotosControllerTest < ActionController::TestCase

  test "should get show" do
    photo = photos(:admin_photo)
    get :show, id: photo.id
    assert_response :success
  end

  test "should edit" do
    user = users(:admin)
    signin(user)
    photo = photos(:admin_photo)
    get :edit, id: photo.id
    assert_response :success
  end

  test "should redirect to root before sign in" do
    photo = photos(:admin_photo)
    get :edit, id: photo.id
    assert_redirected_to(controller: "home")
  end

  test "should raise RoutingError if not correct user when edit a photo" do
    user = users(:normal)
    signin(user)
    photo = photos(:admin_photo)
    assert_raises(ActiveRecord::RecordNotFound) {
      get :edit, id: photo.id
    }
  end

  private
    def signin(user)
      cookies[:auth_token] = user.name
    end
end
