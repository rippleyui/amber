require 'test_helper'

class SessionsControllerTest < ActionController::TestCase

  test "should return douban" do
    request.env['omniauth.auth'] = OmniAuth.config.mock_auth[:douban]
    auth = request.env['omniauth.auth']
    assert_equal("douban", auth["provider"])
    assert_response :success
  end

  test "should create a new user" do
    request.env['omniauth.auth'] = OmniAuth.config.mock_auth[:douban]
    assert_difference('User.count') do
      post :create
    end
    new_user = User.last
    assert_not_nil(new_user.auth_token)
    assert_redirected_to(setup_user_path(new_user))
  end
end
