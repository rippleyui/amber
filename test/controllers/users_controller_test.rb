require 'test_helper'

class UsersControllerTest < ActionController::TestCase

  test "should get show" do
    user = users(:normal)
    assert_equal('v4lour', user.name)
    get :show, id: user.uuid
    assert_response :success
  end

  test "should redirect to home page when edit before sign in" do
    user = users(:normal)
    get :edit, id: user.uuid
    assert_redirected_to(controller: "home")
  end

  test "should get edit" do
    user = users(:normal)
    signin(user)
    get :edit, id: user.uuid
    assert_response :success
  end

  test "should update" do
    user = users(:normal)
    signin(user)
    get :edit, id: user.uuid
    assert_response :success
    user.update(email:"normal@gmail.com")
    assert_response :success
  end

  private
    def signin(user)
      cookies[:auth_token] = user.name
    end
end
