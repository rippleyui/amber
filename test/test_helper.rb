ENV["RAILS_ENV"] ||= "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  ActiveRecord::Migration.check_pending!

  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  fixtures :all

  OmniAuth.config.test_mode = true
  omniauth_hash = { 'provider' => 'douban',
                    'uid' => '87654321',
                    'info' => {
                      'name' => 'ry',
                     }
                 }
  OmniAuth.config.add_mock(:douban, omniauth_hash)
end